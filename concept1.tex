\begin{figure}[h]
\centering
\includegraphics*[width=0.8\columnwidth]{images/image9} 
\caption{The extended screen space metaphor for showing a high resolution inlay of a map on smartwatch inside a low resolution representation on a HMD.}
\label{fig:ESSMap}
\vspace{-.5cm}
\end{figure}

\section{Interaction by dynamic alignment} 
MultiFi aims to reduce the access cost of involving multiple devices in micro-interactions by dynamically leveraging complementary input and output fidelities. The core idea is to use \textit{dynamic alignment} of both the devices and the widgets shown on these devices as an interaction technique. 

Dynamic alignment can be seen as an application of proxemics~\cite{greenberg2011proxemic}: Computers can react to users and other devices based on factors such as distance, orientation, or movement. In MultiFi, dynamic alignment changes the interaction mode of devices based on a combination of proxemic dimensions. We focus on \textit{distance} and \textit{orientation} between devices. However, different alignment styles can be explored, which are location-aware, vary between personal and public displays or consider movement patterns. 

To better understand the design implications of dynamic alignment, we begin with a characterization of the most relevant design factors that we determined %from a literature survey and
 throughout the iterative development process of MultiFi. 

\subsection{Design factors for multiple wearable displays}
\textit{Spatial reference frames} encompass where in space information can be placed, if this information is fixed or movable (with respect to the user) and if the information is a tangible physical representation (i.e., if the virtual screen space coincides with a physical screen space)~\cite{ens2014ethereal}. 

\textit{Direct vs. indirect input}. We use the term direct input, if input and output space are spatially registered, and indirect input, if they are separated. As a consequence of allowing various spatial reference frames, both direct and indirect input must be supported. 

\textit{Fidelity} of individual devices concerns the quality of output and input channels such as spatial resolution, color contrast of displays, focus distance, or achievable input precision. We also understand the display size as fidelity factor, as it governs the amount and hence quality of information that can be perceived from a single screen. 

\textit{Continuity}. The ease of integrating information across several displays not only depends on the individual display fidelities, but also on the quality difference or gap between those displays, in particular, if interaction moves across display boundaries. Continuity of the spatial reference frame describes if the information space is continuous, as with virtual desktops, or discrete, e.g., when virtual display areas are bound to specific body parts~\cite{chen2012extending}. 

Continuity factors pose potential challenges when combining multiple on and around the body displays. For example, combining touchscreen and HMD extend the output beyond a physical screen of a smartwatch, but not the input. This leads to potential interaction challenges, when users associate the extension of output space with an extension of the input space. 

\textit{Social acceptability} of interactions with mobile, wearable, on and around body devices have been extensively studied~\cite{Rico2009}, revealing the personal and subjective nature of what is deemed acceptable. This varies due to many factors including the technology, social situation or location. 
Dynamic alignment allows for some degree of interaction customization, allowing people to tailor their interactions in a way which best suits the current context, rather than having to rely on default device patterns which may be wholly unsuited to the context of use. 

\subsection{Design factors for dynamic alignment of devices}
\textit{Alignment modes}. For the combination of HMD, smartwatch and smartphone, we distinguish three possible alignment modes:
\textit{Non-aligned} devices act as if there was no other device to interact with. 
\textit{Side-by-side alignment} redirects input or output from one device to the other. For example, the HMD serves as the main output device, and the information space is view-referenced. The touchscreen serves solely as an indirect input device (touchpad). A side-by-side configuration can be automatically activated, if widgets are displayed in the HMD, and the view frustrum of the HMD does not coincide with the screen-space of the touch device.
\textit{Fully aligned} devices share a common, spatially registered information space. A touch screen used as direct input device can facilitate direct spatial selection of elements presented in the HMD or on the touch screen itself. While wearable information displays could be placed anywhere in the 3D space around the body, we focus on widgets in planar spaces, as suggested by Ens et al.~\cite{ens2014ethereal}.  

\textit{Roles of devices} depend on whether the touchscreen or the HMD determine the primary information space. 
If the touchscreen is the primary information space, the information display is spatially registered to the touchscreen device and moves with it. The HMD adds additional, peripheral information at lower fidelity. 
If the HMD is the primary information space, it can act as low fidelity viewing device into a {body-referenced} information space, allowing to obtain a fast overview. In this configuration, the touchscreen serves both as high fidelity viewing device as well as direct input device allowing combined spatial and touch interaction within its confines. 

\textit{Visual vs. semantic level of detail}
An additional design decision to consider for full alignment of displays with different fidelities is the purpose to which the higher fidelity display it put: The first option is to solely display information at higher quality. For example, the user could align a touch screen over a label to improve the readability of text. The second option presents \textit{semantic level of detail}~\cite{perlin1993pad}, revealing additional information through a magic lens metaphor~\cite{bier1993toolglass}. Here, the widget changes appearance to show additional information.  For example, in Figure~\ref{fig:hhdlist}, the ``Bedroom'' label turns into a scrollable list, once the borders of the smartphone and the label are spatially aligned. Similarly, in Figure \ref{fig:f4} (bottom row), a smartphone show a richer variation of a widget group including photos and detailed text, once it is aligned with the low fidelity representation on the user's arm.

\subsection{Widgets and interaction techniques}
%\todo[inline]{this section still mixes widgets with interaction techniques - maybe rename the section?}
Our toolkit aims to provide developers with multi-fidelity user interface widgets. MultiFi widgets can adapt their behavior to the current alignment of devices. For example, widgets can relocate from one device to the other, if a certain interaction fidelity is required. We have identified a number of ways how existing widgets can be adapted across displays. Here we demonstrate four widgets to exemplify our concepts. 

\textbf{Ring menus and cascaded lists} On smartwatches, menu and lists widgets can only show a few items at once due to limited screen space. We use an HMD to extend the screen space of the smartwatch, so users get a quick preview of neighboring list items (Figure \ref{fig:f1}, middle). Similarly, list widgets on an HMD can adapt their appearance to show more information once a handheld device is aligned (Figure \ref{fig:hhdlist}).

%JG: more detailed repetition of what is written above:
%Consider a list view of house prices, which only shows a coarse icon of the house on the smartwatch. As soon as a second device is aligned, it can leverage the opportunity to display a more detailed representation, such as a high resolution webpage (Figure~\ref{fig:hhdlist}). 

\begin{figure}
\centering
\includegraphics*[width=2.89in, height=1.78in, keepaspectratio=false]{images/spatialpointing}
\caption{Spatial pointing via a smartphone triggers a low fidelity widget on the HMD to appear in high fidelity on the smartphone.}
\label{fig:hhdlist}
\vspace{-0.5cm}
\end{figure}

\textbf{Map} Navigation of large maps is often constrained by screen space. We introduce two map widgets that combine a HMD and a touch screen display. The first map widget works similar to the list widget, but extends the screen space of a touch display in both directions. Interaction is achieved via the touch display. 

The second variant makes use of a body-referenced information space. The map is displayed in the HMD relative to the upper body, either horizontally, vertically or tilted (Figure~\ref{fig:ESSMap}). Selection of map items can be accomplished through spatial pointing with the smartwatch. The touch device can be used as a combined spatial pointing and touch device for information spaces visible in the HMD. %(Figure~\ref{fig:f5})
If the map size is larger than the virtual display space, the touchpad on the smartwatch provides additional pan and zoom operations.

%JG: out of place: either move somewhere else or ditch
%Given a low fidelity overview in an HMD, the user can coarsely select one element on a map via spatial pointing with a smartphone. Once the alignment is recognized (the user's viewpoint, the smartphone and the chosen element are aligned on a ray), the HMD blanks the area around the element to let the smartphone display a high resolution inset. The user may interact with the widget via touchscreen (Figures~\ref{fig:ESSMap},~\ref{fig:hhdlist}). 

%JG: same here - out of place: either move somewhere else or ditch
%To mitigate effects of accidental alignments, appearance adaption can be based on duration. Users can also tear the widget attached to the smartwatch away from the body-referenced or head-referenced information space. A rubberband effect snaps the widget back to alignment once the user is done interacting with it. This approach overcomes limitations of previous work, which required users to either focus on the physical object or on a separate display for selection \cite{delamare2013designing}.

%\begin{figure}
%\centering
%\includegraphics*[width=2.48in, height=1.57in, keepaspectratio=false, trim=0.87in 0.26in 0.52in 0.34in]{images/image7}
%\caption{The extended screen space metaphor for showing a map across smartphone and HMD.}
%\label{fig:f3}
%\end{figure}

\textbf{Arm clipboard} Existing body-centric widgets for handheld devices \cite{chen2012extending, li2009virtual} rely on proprioceptive or kinesthetic memorization, because the field of view of the handheld is small. With an additional HMD, users can see where on their body they store and retrieve information with their handheld device. If a list widget displays additional information on one side of the smartwatch (overview+detail), we can let users store selected items on their lower arm (Figure~\ref{fig:f4}). Aligning the smartphone with one of the items stored on the arm automatically moves the item to the higher fidelity smartphone. For prolonged interaction, the item can now be manipulated with two hands on the smartphone. Through the combination of HMD for overview and touch enabled displays for selection and manipulation, body-referenced information spaces could become more accessible compared to previous approaches solely relying on proprioceptive memory \cite{chen2012extending}.

\textbf{Text input} We have implemented a full-screen soft keyboard (Figure~\ref{fig:f1}, right) for a smartphone used with an HMD. The additional screen real estate on the smartphone allows to enlarge the soft keys significantly, while the text output is redirected to the HMD.

As soon as a HMD is aligned, the text output area can relocate from one device to the other (see Figure \ref{fig:f1}, right). This would result in two potential benefits. First, the larger input area could help speed up the writing process. Second, the written text is not publicly visible, hence supporting privacy. 

\textbf{Selection beyond touch screen area} While the screen space of a touch device is extended, the input space remains restricted to the the physical screen. To mitigate this limitation, users can quickly switch between touch screen-referenced and body-referenced interaction. This is specifically beneficial for small touch screens to avoid tedious touch based scrolling. At the press of a button (or dwell gesture), the information space can be fixed in air at the current position. Then users can physically select a new area of the information space by physical pointing, making it tangible again.

\begin{figure}[h]
\centering
\includegraphics*[width=2.89in, height=1.78in, keepaspectratio=false]{images/image8}
\caption{Arm clipboard with extended screen space for smartwatch list items and low fidelity widgets. Spatial pointing enables switching to high fidelity on a HHD.}
\label{fig:f4}
\vspace{-0.3cm}
\end{figure}

%\subsection{Application scenario}
%We will use the following scenario to demonstrate our ideas: John is looking to buy a new house. At home, he uses an HMD to get a quick overview on a map in front of his body (Figure~\ref{fig:ESSMap}). He narrows down his choices by quickly selecting filter settings with his smartphone and HMD (Figure~\ref{fig:f5}). He goes out to check out a particular house and on its way can further select points of interest on the body-centric map with his smartwatch (Figure~\ref{fig:ESSMap}). At the house, he compares it with other offers, using his arm as a clipboard (Figure~\ref{fig:f4}). He actually favors the house just visited and informs his real estate agent by email using a full screen keyboard on his smartphone (Figure~\ref{fig:f1}, right).



