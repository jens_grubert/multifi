\begin{figure}[h]
\centering
\includegraphics*[width=\columnwidth]{images/image9} 
\caption{The extended screen space metaphor for showing a high resolution inlay of a map on SW inside a low resolution representation on a HMD.}
\label{fig:ESSMap}
\vspace{-.3cm}
\end{figure}


% DS - another change to make it fit the column...
Unlike prior work, we focus on the dynamic alignment of multiple body-worn displays, using body motion for spatial interaction. 

\section{Interaction by dynamic alignment} 

MultiFi aims to reduce the access cost of involving multiple devices in micro-interactions by dynamically leveraging complementary input and output fidelities. We propose \textit{dynamic alignment} of both devices and widgets shown on these devices as an interaction technique. 

Dynamic alignment can be seen as an application of proxemics~\cite{greenberg2011proxemic}: Computers can react to users and other devices based on factors such as distance, orientation, or movement. In MultiFi, dynamic alignment changes the interaction mode of devices based on a combination of proxemic dimensions. We focus on \textit{distance} and \textit{orientation} between devices. However, different alignment styles can be explored, which are location-aware, vary between personal and public displays or consider movement patterns. 

\subsection{Design factors}
To better understand the design implications of dynamic alignment, we begin with a characterization of the most relevant design factors determined %from a literature survey and 
throughout the iterative development process of MultiFi. 

\textit{Spatial reference frames} encompass where in space information can be placed, if this information is fixed or movable (with respect to the user) and if the information has a tangible physical representation (i.e., if the virtual screen space coincides with a physical screen space)~\cite{ens2014ethereal}. 

\textit{Direct vs. indirect input}. We use the term direct input, if input and output space are spatially registered, and indirect input, if they are separated. As a consequence of allowing various spatial reference frames, both direct and indirect input must be supported. 

\textit{Fidelity} of individual devices concerns the quality of output and input channels such as spatial resolution, color contrast of displays, focus distance, or achievable input precision. We also understand the display size as a fidelity factor, as it governs the amount and hence quality of information that can be perceived from a single screen. 

\textit{Continuity}. The ease of integrating information across several displays not only depends on the individual display fidelities, but also on the quality difference or gap between those displays, in particular, if interaction moves across display boundaries. We call this \textit{continuity of fidelity}. In addition, \textit{continuity of the spatial reference frame} describes if the information space is continuous, as with virtual desktops, or discrete, e.g., when virtual display areas are bound to specific body parts~\cite{chen2012extending}. 
Continuity factors pose potential challenges when combining multiple on and around the body displays. For example, combining touch screen and HMD extends the output beyond a physical screen of a SW, but not the input. This leads to potential interaction challenges, when users associate the extension of the output space with an extension of the input space. 

\textit{Social acceptability} of interactions with mobile, on and around body devices have been extensively studied~\cite{Rico2009}, revealing the personal and subjective nature of what is deemed acceptable. This varies due to many factors including the technology, social situation or location. 
Dynamic alignment allows for some degree of interaction customization, allowing people to tailor their interactions in a way which best suits their current context, rather than having to rely on default device patterns which may be wholly unsuited to the context of use. 

\subsection{Alignment modes}
For the combination of HMD and touch device, we distinguish three possible alignment modes (see Figure \ref{fig:dabass}):

In \textit{body-aligned mode}, 
the devices share a common information space, which is spatially registered to the user's body (Figure \ref{fig:dabass}, left). While wearable information displays could be placed anywhere in the 3D space around the body, we focus on widgets in planar spaces, as suggested by Ens et al.~\cite{ens2014ethereal}.  
The HMD acts as a low fidelity viewing device into a body-referenced information space, allowing one to obtain a fast overview. The touchscreen provides a high fidelity inset, delivering detail-on-demand, when the user points to a particular location in the body-referenced space. Also, in contrast to common spatial pointing techniques, the touchscreen provides haptic input into the otherwise intangible information space. 

In \textit{device-aligned mode} ,
the information space is spatially registered to the touchscreen device and moves with it (Figure \ref{fig:dabass}, middle). The HMD adds additional, peripheral information at lower fidelity, thus \textit{extending the screen space} of the touch screen, yielding a focus+context display.

In \textit{side-by-side mode}, 
interaction is redirected from one device to the other without requiring a spatial relationship among devices (Figure \ref{fig:dabass}, right). For example, if the HMD shows a body-referenced information space, a touch device can provide indirect interaction. The touch device can display related information, and input on the touch device can affect the body-referenced display. If the touch device is outside the user's field of view, the touch screen can still be operated blindly.   

\begin{figure}
\centering
\includegraphics*[width=\columnwidth]{images/da-ba-ss3}
\caption{In body-aligned mode (left) devices are spatially registered in a shared information space relative to the user's body. In device-aligned mode (middle) the screen space of the touchscreen is extended. In side-by-side mode (right) devices have separated information spaces and do not require a spatial relationship.}
\label{fig:dabass}
\vspace{-0.5cm}
\end{figure}

\subsection{Navigation}
The principal input capabilities available to the user are spatial pointing with the touch device, or using the touch screen. Spatial pointing with the touch device is a natural navigation method in body-aligned mode. Once the alignment is recognized (the user's viewpoint, the handheld and the chosen item are aligned on a ray), the HMD clears the area around the element to let the handheld display a high resolution inset. This navigation method can be used for selection or even drag-and-drop in the body-referenced information space. However, extended use can lead to fatigue. 

Spatial pointing in device-aligned mode can be seen as a more indirect form of navigation, which allows one to obtain a convenient viewpoint on the device-aligned information space. Navigation of the focus area will naturally be done by scrolling on the touch screen, but this can be inefficient, if the touch screen is small. %While, scrolling by touch could also be used in body-aligned mode, but makes more sense for a side-by-side mode.
Hence, users may mitigate the limitation of input to the physical screen with a clutch gesture that temporarily switches to body-aligned mode. At the press of a button (or dwell gesture), the information space can be fixed in air at the current position. Then users can physically select a new area of the information space by physical pointing, making it tangible again.

\subsection{Focus representation and manipulation}
An additional design decision is the representation shown on the higher fidelity display: The first option is to solely display a higher \textit{visual level of detail}. For example, the user could align a touch screen over a label to improve the readability of text (Figure~\ref{fig:ESSMap}). The second option presents \textit{semantic level of detail}~\cite{perlin1993pad}, revealing additional information through a magic lens metaphor~\cite{bier1993toolglass}. Here, the widget changes appearance to show additional information.  For example, in Figure~\ref{fig:hhdlist}, the ``Bedrooms'' label turns into a scrollable list, once the borders of the handheld and the label represenation in the HMD are spatially aligned. Similarly, in Figure \ref{fig:f4} (bottom row), a handheld shows a richer variation of a widget group including photos and detailed text, once it is aligned with the low fidelity representation on the user's arm.

%\textit{Focus manipulation}.
An interactive focus representation on the touch device can naturally be operated with standard touch widgets. In body-aligned mode, this leads to a continuous coarse-to-fine \textit{cascaded interaction}: The user spatially points to an item with a low fidelity representation and selects it with dwelling or a button press. A high fidelity representation of the item appears on the touch screen and can be manipulated by the user through direct touch (Figures~\ref{fig:ESSMap},~\ref{fig:hhdlist},~\ref{fig:f4}).

 %with a large touchscreen through \textit{cascaded interaction}. In a first stage a low fidelity information item (such as 2x2 grid) could be selected through spatial pointing. In a second stage the representation could change into a higher fidelity item (e.g. 4x4 grid) and the final selection could be accomplished through touch screen selection.
%In body-aligned mode, this leads to a continuous coarse-to-fine interaction: The user spatially points to an items and selects it with dwelling or a button press. An interactive representation of the items appears on the touch screen and can be manipulated by the user (Figures~\ref{fig:ESSMap},~\ref{fig:hhdlist}).

For simple operations, this can be done directly in body-aligned mode. For example, widgets such as checkbox groups may be larger than the screen of a SW, but individual checkboxes can be conveniently targeted by spatial pointing and flipped with a tap. 
However, holding the touch device still at arm's length or at awkward angles may be demanding for more complex operations. In this case, it may be more suitable to \textit{tear off} the focus representation from the body-aligned information space by automatically switching to side-by-side mode. A rubberband effect snaps the widget back into alignment, once the user is done interacting with it. This approach overcomes limitations of previous work, which required users to either focus on the physical object or on a separate display for selection \cite{delamare2013designing}.

\begin{figure}
\centering
\includegraphics*[width=\columnwidth]{images/spatialpointing}
\caption{Spatial pointing via a handheld triggers a low fidelity widget on the HMD to appear in high fidelity on the handheld.}
\label{fig:hhdlist}
\vspace{-0.5cm}
\end{figure}

\subsection{Widgets and applications}
MultiFi widgets adapt their behavior to the current alignment of devices. For example, widgets can relocate from one device to the other, if a certain interaction fidelity is required. We have identified a number of ways how existing widgets can be adapted across displays. Here we discuss several widget designs and applications employing such widgets to exemplify our concepts. 

\textbf{Menus and lists:} On a SW, menu and list widgets can only show a few items at once due to limited screen space. We use an HMD to extend the screen space of the SW, so users get a quick preview of nearby items in a \textit{ring menu}, Figure \ref{fig:f1}, middle. Similarly, list widgets on an HMD can adapt their appearance to show more information once a handheld device is aligned, Figure \ref{fig:hhdlist}.

\textbf{Interactive map:} Navigation of large maps is often constrained by screen space. We introduce two map widgets that combine HMD and touch screen. The first map widget works similar to the list widget, but extends the screen space of a touch display in both directions. Interaction is achieved via the touch display. 

The second variant makes use of a body-referenced information space. The map is displayed in the HMD relative to the upper body, either horizontally, vertically or tilted (Figure~\ref{fig:ESSMap}). If the map size is larger than the virtual display space, the touchpad on the SW provides additional pan and zoom operations.

%\begin{figure}
%\centering
%\includegraphics*[width=2.48in, height=1.57in, keepaspectratio=false, trim=0.87in 0.26in 0.52in 0.34in]{images/image7}
%\caption{The extended screen space metaphor for showing a map across smartphone and HMD.}
%\label{fig:f3}
%\end{figure}

\textbf{Arm clipboard:} Existing body-centric widgets for handhelds \cite{chen2012extending, li2009virtual} rely on proprioceptive or kinesthetic memorization, because the field of view of the handheld is small. With an additional HMD, users can see where on their body they store through head pointing and subsequently retrieve information with a handheld device. If a list widget displays additional information on one side of the SW (overview+detail), we can let users store selected items on their lower arm (Figure~\ref{fig:f4}). Aligning the handheld with one of the items stored on the arm automatically moves the item to the higher fidelity handheld. For prolonged interaction, the item can now be manipulated with two hands on the handheld. Through the combination of HMD for overview and touch enabled displays for selection and manipulation, body-referenced information spaces could become more accessible compared to previous approaches solely relying on proprioceptive memory \cite{chen2012extending, li2009virtual}.

\textbf{Text input:} Using MultiFi text widgets, we have implemented a full-screen soft keyboard application for a handheld used with a HMD. The additional screen real estate on the handheld allows MultiFi to enlarge the soft keys significantly, while the text output is redirected to the HMD.
As soon as a HMD is aligned, the text output area can relocate from one device to the other (see Figure \ref{fig:f1}, right). This results in two potential benefits. First, the larger input area could help speed up the writing process. Second, the written text is not publicly visible, hence supporting privacy. 

\begin{figure}[h]
\centering
\includegraphics*[width=\columnwidth]{images/image8}
\caption{Arm clipboard with extended screen space for low fidelity widgets (top). Spatial pointing enables switching to high fidelity on a handheld (bottom).}
\label{fig:f4}
\vspace{-0.3cm}
\end{figure}

%\subsection{Application scenario}
%We will use the following scenario to demonstrate our ideas: John is looking to buy a new house. At home, he uses an HMD to get a quick overview on a map in front of his body (Figure~\ref{fig:ESSMap}). He narrows down his choices by quickly selecting filter settings with his smartphone and HMD (Figure~\ref{fig:f5}). He goes out to check out a particular house and on its way can further select points of interest on the body-centric map with his smartwatch (Figure~\ref{fig:ESSMap}). At the house, he compares it with other offers, using his arm as a clipboard (Figure~\ref{fig:f4}). He actually favors the house just visited and informs his real estate agent by email using a full screen keyboard on his smartphone (Figure~\ref{fig:f1}, right).
