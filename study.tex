\section{User study}
 %\todo[inline]{improve the readability: remove extensive descriptive statistics, make figures better readable}
%\todo[inline]{figures better readable}

We conducted a laboratory user study to investigate if combined device interaction can be a viable alternative to established single device interaction for mobile tasks.
%\todo[inline]{make clearer  motivation to initially focus on atomic tasks}
For the study we concentrated on two atomic tasks: information search and selection. Those tasks were chosen as they can be executed on the go and underpin a variety of more complex tasks. 

\subsection{Experimental design}
We designed a within-subjects study to compare performance and user experience aspects of MultiFi interaction to single device interaction for two low level tasks. We complemented the focus on these atomic tasks with user inquiries about the potential and challenges of joint on and around the body interaction. For both tasks, we report on the following dependent variables: \textit{task completion time}, \textit{errors, subjective workload} as measured by NASA TLX \cite{hart1988development}
 as well as user experience measures (After Scenario Questionnaire (ASQ) \cite{lewis1991psychometric},
\textit{hedonic and usability aspects} as measured by AttrakDiff \cite{hassenzahl2003attrakdiff}) 
and overall \textit{preference }(ranking). The independent variable for both tasks was interface with five conditions:

\textit{Handheld}: The Samsung Galaxy SIII was used as only input and output device. This serves as the baseline for a handheld device with high input and output fidelity. 

\textit{Smartwatch (SW):} The wrist-worn Sony Xperia Z1 compact was used as only input and output device. The input and output area was 40x35 mm and highlighted by a yellow border, as shown in Figure~\ref{fig:ESSMap}. Participants were notified by vibration if they touched outside the input area. This condition serves as baseline for a wearable device with low input and output fidelity (high resolution, but small display space). 

\textit{Head Mounted Display (HMD):} The Vuzix STAR 1200XL was used as an output device. We employed indirect input as in the SW condition using a control-display ratio of 1 with the touch area limited to the central screen area of the HMD. This condition serves as the baseline for a HMD with low input and output fidelity, which can be operated with an arm-mounted controller.
% (without the need for retrieving the controller from a pocket). 

%\todo[inline]{move the following two paragraphs to the concept section?}
% DS: I have just shortened it.
\textit{Body-referenced interaction (BodyRef):} The content was displayed in front of the participant in body-aligned mode with additional touch scrolling. Selection was achieved by aligning the smartwatch with the target visible in front of the user and touching the target rendered on the smartwatch.

\textit{Smartwatch referenced (SWRef):} The information space was displayed in device-aligned mode (Figure~\ref{fig:f9}). All other aspects were as in BodyRef.

\subsection{Apparatus and data collection}
%\todo[inline]{shorten and move parts to implementation}
The study was conducted in a controlled laboratory environment. The devices employed were the ones described in the implementation section. The translation of virtual cameras for panning via touch in all conditions parallel to the screen was set to ensure a control-display ratio of 1. Pinch to zoom was implemented by the formula 
$s = s_0 \cdot s_g$, with $s$ being the new scale factor, $s_0$ the map's scale factor at gesture begin and $s_g$ the relation between the finger distances at gesture begin and end. While the system is intended for mobile use, here participants conducted the tasks while seated at a table (120x90 cm, height 73 cm, height adjustable chair) due to the strenuous nature of the repetitive tasks in the study. %The participants were seated on a table (120x90 cm, height 73 cm). The chair was height adjusted for individual participants to ensure that its armrests are at the same height as the table. This should mitigate expected fatigue effects which could arise during the repetitive nature of the tasks. We collected data for evaluation through automatic logging on the test devices, questionnaires, video recording and semi-structured interviews at the end of the study. %For data analysis, we used R and SPSS. 
Null hypothesis significance tests were carried out at a .05 significance level, and no data was excluded, if not otherwise noted. For ANOVA (repeated measures ANOVA or Friedman ANOVA), Mauchly's test was conducted. If the sphericity assumption had been violated, degrees of freedom were corrected using Greenhouse-Geisser estimates of sphericity. For post-hoc tests (pairwise t-test or Wilcoxon signed rank) Bonferroni correction was applied. Due to space reasons not all tests statistics are reported in detail, but are available as supplementary material\footnote{http://www.jensgrubert.wordpress.com/research/multifi/}.

%For ANOVA, Mauchly's test was conducted. If the sphericity assumption had been violated, degrees of freedom were corrected using Greenhouse-Geisser estimates of sphericity. Due to space reasons not all tests statistics are reported in detail, but are available in the appendix. For all figures, brackets indicate significant difference with a p-value $<$0.05 (*) and $<$0.01 (**). Error bars indicate standard deviation.

\subsection{Procedure}
After an introduction and a demographic questionnaire, participants were introduced to the first task (counterbalanced) and the first condition (randomized). For each condition, a training phase was conducted. For each task, participants completed a number of trials (as described in the individual experiment sections) in five blocks, each block for a different condition. Between each block, participants filled out the questionnaires. At the end of the study, a semi-structured interview was conducted and participants filled out a separate preference questionnaire. Finally, the participants received a book voucher worth 10 Euros as compensation. Participants were free to take a break between individual blocks and tasks. Overall, the study lasted ca. 100 minutes per participant. 

\subsection{Participants}
Twenty-six participants volunteered in the study. We had to exclude three participants due to technical errors (failed tracking or logging). In total, we analyzed data from twenty-three participants (1 f, average age: 26.75 y, $\sigma $=5.3, average height: 179 cm, $\sigma $= 6, 7 users wore glasses, three contact lenses, 2 left-handed users). All but one user were smartphone owners (one less than a year). Nobody was a user of smartwatches or head-mounted displays. Twenty users had a high interest in technology and strong computer skills (three medium).

\subsection{Hypotheses}
One of our main interests was to investigate if combined display interaction could outperform interaction with individual wearable devices. We included Handheld interaction as a baseline and did not expect the combined interfaces to outperform it. Hence, we had the following hypotheses: \textit{H1:} Handheld will be fastest for all tasks. \textit{H2:} BodyRef will be faster than HMD and SW (ideally close to Handheld). \textit{H3:} BodyRef will result in fewer errors than HMD and SW. \textit{H4:} SWRef will be faster than HMD and SW (ideally close to Handheld). \textit{H5:} SWRef will result in fewer errors than HMD and SW. 

\section{Experiment 1: Locator task on map}
A common task on mobile mapping applications is to search for an object with certain target attributes \cite{reichenbacher2001adaptive}. We employed a locator task similar to previous studies involving handheld devices and multi-display environments \cite{grubert2014utility,rashid2012cost}. Participants had to find the lowest price label (text size 12 pt) among five labels on a workspace size of 400x225 mm. We determined the workspace size empirically, to still allow direct spatial pointing for the BodyRef condition. While finding the lowest price could easily be solved with other widgets (such as a sortable list view), our task is only an instance of general locator tasks, which can encompass non-quantifiable attributes such as textual opinions of users, which cannot be sorted automatically. Users conducted ten trials per condition. With 23 participants, five interface levels and 10 trials, there was a total of 23x5x10=1150 trials.

\subsection{Task completion time and errors}
The task completion times (TCT, in seconds), for the individual conditions can be seen in Figure \ref{fig:tctloc}. %were as follows: Handheld (M=15.67, $\sigma $=5.45), SW (M=20.60, $\sigma $=7.62), HMD (M=18.68, $\sigma $=6.45), BodyRef (M=16.57, $\sigma $=6.16), SWRef (M=21.05, $\sigma $=10.28). 
A repeated measures ANOVA indicated that there was a significant effect of interface on TCT, F(3.10, 709.65)=42.21, p$<$.001. Post-hoc tests indicated that both Handheld and BodyRef were significantly faster than all remaining interfaces with medium to large effect sizes (see also Figure \ref{fig:tctloc}). HMD was significantly faster than both SW and SWRef. There were no significant differences between Handheld-BodyRef and SW-SWRef. %are depicted in Table 1. Pairs are column-wise (e.g., pair Handheld-SW: \textit{t}=-11.0, p=$<$.01, \textit{d}=-.72). Significant differences are highlighted in bold. For all tests, degrees of freedom were 229. To summarize, both Handheld and BodyRef were significantly faster than all remaining interfaces with medium to large effect sizes. HMD was significantly faster than both SW and SWRef. There were no significant differences between Handheld-BodyRef and SW-SWRef. The smaller standard deviations of Handheld, SW and HMD compared to BodyRef and SWRef could be attributed to the longer familiarity of users with touch screen interaction compared to the novel interfaces we introduced.

From 230 selections, eight false selections were made in the Handheld, HMD and BodyRef conditions. In the SW condition, 13 errors have been made, in SWRef five errors. No significant differences were found.%A Friedman ANOVA indicated no significant effect of interface on errors $\chi^2$(4)=4.10, p=.39.

\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth, keepaspectratio=false]{images/map-tct}
\vspace{-.2cm}
\caption{Task completion time (s) for the locator task.}
\label{fig:tctloc}
\vspace{-.5cm}
\end{figure}
%\begin{table}
%\centering
%\begin{tabular}{|p{0.3in}|p{0.6in}|p{0.55in}|p{0.55in}|p{0.55in}|} \hline 
%\textbf{\tiny t, p, d} & \textbf{\tiny Handheld} & \textbf{\tiny SW} & \textbf{\tiny HMD} & \textbf{\tiny BodyRef} \\ \hline 
%{\tiny Handheld} & - &  &  &  \\ \hline 
%{\tiny SW} & \textbf{\tiny -11.0, $<$.01, -.72} & - &  &  \\ \hline 
%{\tiny HMD} & \textbf{\tiny -7.4, $<$.01, -.49} & \textbf{\tiny 3.6, .004, .24} & - &  \\ \hline 
%{\tiny BodyRef} & {\tiny -2.3, .22, -.15} & \textbf{\tiny 8.4, $<$.01, .56} & \textbf{\tiny 5.1, $<$.01, .33} & - \\ \hline 
%{\tiny SWRef} & \textbf{\tiny -8.8, $<$.01, -.59} & {\tiny -1.1, 1.0, -.07} & \textbf{\tiny -4.1, $<$.01, .27} & \textbf{\tiny -7.3, $<$.01, -.48} \\ \hline 
%\end{tabular}
%\caption{Test statistics (t-value, p-value, Cohen's d, paired sample t-test with Bonferroni correction) for the map task.}

%\end{table}

%\subsection{Errors}
%From 230 selections, eight false selections were made in the Handheld, HMD and BodyRef conditions. In the SW condition, 13 errors have been made, in SWRef five errors. A Friedman ANOVA indicated no significant effect of interface on errors $\chi $2\eqref{GrindEQ__4_}=4.10, p=.39.

\subsection{Subjective workload and user experience}
%The subjective workload scores for individual dimensions as measured by the NASA TLX are depicted in Figure~\ref{fig:f7}. 
Repeated measures ANOVAs indicated that there were significant effects of interface on all dimensions. Post-hoc tests indicated that BodyRef resulted in a higher mental demand than smartwatch (albeit with a small effect size). The handheld condition resulted in significantly lower subjective workload for all other dimension compared to most other interfaces. The analysis of the ASQ (repeated measures ANOVA and post-hoc tests) indicated that for Handheld ease of task was significantly higher than for SWRef. Analysis of AttrakDiff showed that all interfaces scored slightly below average for pragmatic quality (PQ), see Figure \ref{fig:f8}, and only a significant difference between HMD-SWRef could be found (but with a small effect size). For hedonic quality stimulation (HQ-S), the Handheld and SW interface were rated significantly lower than the other three conditions. Preference analysis showed that Handheld (MD=2, M=1.13, $\sigma $=1.13) was significantly more preferred than SW (MD=4, M=3.87, $\sigma $=1.10), Z=-4.25, p$<$.001.

\begin{figure}[hb]
\vspace{-.2cm}
\includegraphics[width=3.35in, height=1.77in, keepaspectratio=false]{images/attrakdiff}
\vspace{-.5cm}
\caption{Pragmatic Quality (PQ) and Hedonic Quality Stimulation (HQS) measures (normalized range -2..2) for the locator task (left) and the select task (right).}
\label{fig:f8}
\vspace{-.3cm}
\end{figure}

%\subsection{User experience}

%Results of the After Scenario Questionnaire (seven item Likert scale, 1: totally disagree, 7: totally agree) can be found in Figure~\ref{fig:f8}. Friedman ANOVA indicated significant effect of interface on ease of task ($\chi $�\eqref{GrindEQ__4_}=26.65, p$<$.001), satisfaction with task completion time ($\chi $�\eqref{GrindEQ__4_}=9.57, p=.048) and system support ($\chi $�\eqref{GrindEQ__4_}=12.20, p=.02). However, Wilcoxon signed rank tests with Bonferroni corrections only indicated a significant difference between Handheld and SWRef for ease of task (Z=-3.36, p=.01).


%\begin{figure}
%\includegraphics*[width=3.31in, height=2.11in, keepaspectratio=false]{images/f6}
%\caption{NASA TLX scores for the locator tasks.}
%\label{fig:f6}
%\end{figure}

%\begin{figure}
%\includegraphics*[width=3.33in, height=1.51in, 
%keepaspectratio=false]{images/f7}
%\caption{ASQ ratings for locator task (7-point Likert)}
%\label{fig:f7}
%\end{figure}

%A repeated measures ANOVA indicated that there was a significant effect of interface on Pragmatic Quality (PQ), F(4, 88)=4.05, p$<$.001 and on Hedonic Quality Stimulation (HQS), F(2.84 , 62.58)=58.26, p$<$.001. For PQ results of post-hoc tests with Bonferroni corrections indicated significant differences as depicted in Figure~\ref{fig:f9}, left.

%Preference ratings (ranking: 1: most preferred 5: least preferred) were as follows. Handheld: MD=2, M=1.13, $\sigma $=1.13, SW: MD=4, M=3.87, $\sigma $=1.10, HMD: MD=2, M=2.78, $\sigma $=1.41, BodyRef: MD=4, M=3.22, $\sigma $=1.41, SWRef: MD=3, M=3.09, $\sigma $=1.38. A Friedman ANOVA indicated that there was a significant effect of interface on preference ($\chi^2$(4)=19.35, p=.001). Wilcoxon signed rank tests with Bonferroni corrections indicated a significant difference between Handheld and SWRef (Z=-4.25, p$<$.001).

%To summarize, for Handheld the ease of task was significantly higher than for SWRef, all interfaces scored slightly below average for pragmatic quality, and only a significant difference between HMD-SWRef could be found (but with a small effect size). For hedonic quality stimulation the Handheld and SW interface were rated significantly lower than the other three conditions. Handheld was significantly more preferred than SW.

\section{Experiment 2: 1D target acquisition}
We employed a discrete 1D pointing task similar to the one used by Zhao et al. \cite{zhao2014model} (Figure~\ref{fig:f5}). Participants navigated to a target (green stripe) in each trial using touch input (for Handheld, SW, HMD, SWRef) or spatial pointing (BodyRef). Final target selection was confirmed by a touch on the target region in all conditions. The participants were asked to use their index finger to interact with the touch surfaces. For each trial, the task was to scroll the background (Handheld, SW, HMD, SWRef) or to move the smartwatch towards the target (BodyRef) until it appeared on the \textit{selection area}. Prior to each trial, participants hit a start button at the center of the screen to ensure a consistent start position and to prevent unintended gestures before scrolling. The target was only revealed after the start button was hit. After successful selection, the target disappeared. For BodyRef, participants returned to a neutral start position centered in front of them before the next trial. In the experiment design we fixed target width to 20 mm (0.5*width of the smartwatch), use the control window and display window sizes of the individual displays and use two target distances (short: 15 cm, long: 30 cm)\footnote{We fixed those parameters as the focus of the experiment was not on generating a new target aquisition model.}.  %In addition to interface and target distance, we also introduced target direction (same side as hand carrying the smartwatch and opposite side), as independent variable as we expected performance differences in the BodyRef condition. 
The conditions were blocked by interface. Per condition, each participant conducted eight trials (plus two training trials). With twenty three participants, five interface levels, two target distances, two directions and eight trials per condition a total of 23x5x2x2x8=3680 trials were conducted. 

%Please note that the focus of this experiment is not to derive a new target acquisition model but rather to get an initial insight into the potential for combined wearable device interaction compared to individual devices only. Hence, in the experiment design, we do not vary all parameters as one would need for deriving a robust model. Specifically, we fix target width to 20 mm (0.5*width of the smartwatch), use the control window and display window sizes of the individual displays and use two target distances (short: 15 cm, long: 30 cm). In addition to interface and target distance, we also introduced target direction (same side as hand carrying the smartwatch and opposite side), as independent variable as we expected performance differences in the BodyRef condition. 

\begin{figure}
\centering
%\includegraphics*[width=1.42in, height=1.0in, keepaspectratio=false]{images/image9} 
\includegraphics*[width=0.8\columnwidth]{images/image10}
\caption{The selection task for SWRef.}
\label{fig:f5}
\vspace{-.5cm}
\end{figure}

\subsection{Task completion time and errors}
Task completion times are depicted in Figure~\ref{fig:f9}. %A repeated measures ANOVA indicated significant interactions between interface and length, F(3.23, 592.25)=89.49, p$<$.001, interface and direction, F(3.27, 599.15)=5.71, p$<$.001 and interface, length, direction, F(2,84, 518.73)=4.58, p$<$.001. 
%Due to space constraints, we report only on the simple main effects of interface across length and direction. 
Repeated measures ANOVAs indicated that for both distances (15~cm, 30~cm) and smartwatch sides (towards and away from dominant hand) interface had a significant effect on TCT. The pairwise significant differences are depicted in Figure~\ref{fig:f9}. Handheld was the fastest interface for both directions and distances. BodyRef was significantly faster than all remaining interfaces. No other significant effects of interface on task completion time were found.

%For short distances (15 cm), interface had a significant effect on TCT, F(3.06, 1122.72)=162.10, p$<$.001, as well as for long distances (30 cm), F(3.13, 1147.80)=267.75, p$<$.001. For selection on the side of the smartwatch (i.e., non-dominant hand side, left for 21 of 23 participants), interface had a significant effect on TCT, F(3.27, 1201.05)=316.35, p$<$.001, as well as for selection on the opposite side of the smartwatch (i.e. dominant hand side), F(3.12, 1145.40)=127.57, p$<$.001. The results of post-hoc comparisons with Bonferroni correction are depicted in Figure~\ref{fig:f10}. To summarize, Handheld was the fastest interface for both directions and distances. BodyRef was significantly faster than all remaining interfaces. No other significant effects of interface on task completion time were found.

%\subsection{Errors}

Selection errors occurred when participants tapped outside the target region. The total number of errors for individual interfaces were as follows: Handheld: 53 (M=.07, $\sigma $=.28), SW: 34 (M=.05, $\sigma $=.23), HMD: 223 (M=.30, $\sigma $=.77), BodyRef: 258 (M=.35, $\sigma $=.78), SWRef: 37 (M=.05, $\sigma $=.24). A Friedman ANOVA indicated that there was a significant effect of interface on error count ($\chi^2(4)$=231.68, p$<$.001). Post-hoc tests indicated significant differences between BodyRef and all interfaces except HMD, as well as between HMD and all interfaces (except BodyRef). %No significant effects of direction or length on error rate were identified. To summarize, HMD and BodyRef resulted in a significant higher error rate.

\begin{figure}[h]
\vspace{-.3cm}
\includegraphics*[width=3.38in, height=1.50in, keepaspectratio=false]{images/select-tct}
\vspace{-.5cm}
\caption{Task completion times (s) for the select task. SWSide: side on which smartwatch was worn, SWOpSide: opposite side. }
\label{fig:f9}
\vspace{-.3cm}
\end{figure}

\subsection{Subjective workload and user experience}
%\todo[inline]{indicate effect size} 
A repeated measures ANOVA indicated that there were significant effects of interface on all dimensions but temporal demand and performance. Post-hoc tests indicated that Handheld resulted in a significantly lower mental demand than most other conditions (except SW) and in a significantly lower overall demand than all conditions. BodyRef and SWRef resulted in significantly higher physical demands compared to Handheld and HMD (but not SW). Frustration was significantly higher for SW and SWRef compared to Handheld. Analysis of results of the ASQ %(Friedman ANOVA and post-hoc Wilcoxon signed rank tests)
 indicated a significant difference between Handheld and SWRef for ease of task (Z= -3.36, p=.01). %Pragmatic Quality (PQ) and Hedonic Quality Stimulation (HQ-S) as measured by AttrakDiff are depicted in Figure~\ref{fig:f9}, right. 
As in the locator task, all interfaces scored below average for PQ-S (see Figure~\ref{fig:f9}). BodyRef and SWRef scored significantly lower than Handheld (indicated by repeated measures ANOVA and post-hoc t-tests). For HQ-S, the Handheld and SW interface were rated significantly lower than the other three conditions as in the locator task.

%To summarize, Handheld scored significantly higher for ease of task and system support compared to BodyRef and SWRef (for system support also compared to SW). As in the locator task, all interfaces scored below average for pragmatic quality. BodyRef and SWRef scored significantly lower than Handheld. For hedonic quality stimulation, the Handheld and SW interface were rated significantly lower than the other three conditions as in the locator task. Handheld was significantly more preferred than SW.


%The subjective workload scores for individual dimensions as measured by the NASA TLX are depicted in Figure~\ref{fig:f11}. A repeated measures ANOVA indicated that there were significant effects of interface on all dimensions but temporal demand and performance. The results of post-hoc tests with Bonferroni corrections indicated significant differences shown in Figure~\ref{fig:f11}. To summarize, Handheld resulted in a lower mental demand than most other conditions (except SW) and in a lower overall demand than all conditions. BodyRef and SWRef resulted in significantly higher physical demands compared to Handheld and HMD (but not SW). Frustration was significantly higher for SW and SWRef compared to Handheld.

%\begin{figure}
%\includegraphics*[width=3.34in, height=1.75in, keepaspectratio=false]{images/f10}
%\caption{NASA TLX scores for the selection tasks.}
%\label{fig:f10}
%\end{figure}
%
%\begin{figure}
%\includegraphics*[width=3.32in, height=1.55in, keepaspectratio=false]{images/f11}
%\caption{ASQ ratings for select task (7-point Likert)}
%\label{fig:f11}
%\end{figure}

%\subsection{User experience}

%Results of the After Scenario Questionnaire (seven item Likert scale, 1: totally disagree, 7: totally agree) can be found in Figure~\ref{fig:f8}. Friedman ANOVAs indicated that there were significant effect of interface on ease of task ($\chi $�\eqref{GrindEQ__4_}=26.65, p$<$.001), satisfaction with task completion time ($\chi $�\eqref{GrindEQ__4_}=9.57, p=.048) and system support ($\chi $�\eqref{GrindEQ__4_}=12.20, p=.02). However, Wilcoxon signed rank tests with Bonferroni corrections only indicated a significant difference between Handheld and SWRef for ease of task (Z= -3.36, p=.01).

%Pragmatic Quality (PQ) and Hedonic Quality Stimulation (HQ-S) as measured by AttrakDiff are depicted in Figure~\ref{fig:f9}, right. A repeated measures ANOVA indicated that there was a significant effect of interface on PQ, F(2.76, 60.69)=4.05, p$<$.001 and on HQ-S, F(4 , 88)=48.45, p$<$.001. For PQ, results of post-hoc tests with Bonferroni corrections indicated significant differences as depicted in Figure~\ref{fig:f9}, right. 

%Preference ratings (ranking: 1: most preferred 5: least preferred) were as follows. Handheld: MD=2, M=2.13, $\sigma $=1.10, SW: MD=5, M=4.09, $\sigma $=1.16, HMD: MD=3, M=2.91, $\sigma $=1.24, BodyRef: MD=3, M=2.78, $\sigma $=1.54, SWRef: MD=3, M=3.09, $\sigma $=1.28. A Friedman ANOVA indicated that there was a significant effect of interface on preference ($\chi $�\eqref{GrindEQ__4_}=17.58, p=.001). Wilcoxon signed rank tests with Bonferroni corrections indicated a significant difference between Handheld and SWRef task (Z=-4.15, p$<$.001).
%
%To summarize, Handheld scored significantly higher for ease of task and system support compared to BodyRef and SWRef (for system support also compared to SW). As in the locator task, all interfaces scored below average for pragmatic quality. BodyRef and SWRef scored significantly lower than Handheld. For hedonic quality stimulation, the Handheld and SW interface were rated significantly lower than the other three conditions as in the locator task. Handheld was significantly more preferred than SW.

\subsection{Qualitative feedback}
In semi-structured interviews participants commented on potentials and limitations of the prototypical MultiFi implementation. 
Most participants (21) commented on the benefits of having an \textit{extended view space} compared to individual touch screens with one participant saying ``\textit{Getting an overview with simple head movements is intuitive and natural}''. %``\textit{Moving your head to get an overview is very intuitive}''. 
Those participants also valued the fact that precise selection was enabled through the smartwatch with one typical comment being ``\textit{The HMD gives you the overview, and the SW lets you be precise in your selection}''. Three participants highlighted the potentially lower \textit{access costs} of MultiFi over smartphones, with one comment being ``\textit{I don’t have to constantly monitor my smartphone}''. In line participants felt that BodyRef interaction was fastest (even though this is not confirmed by the objective measurements). Five participants commented on the benefits of MultiFi over HMD only interaction highlighting the direct interaction or that they could ``\textit{take advantage of proprioception and motion control}''.

Many participants (15) commented on the limitations of the hardware, specifically the quality of the employed HMD with a typical comment being ``\textit{The combined interfaces [SWRef, BodyRef] gave me trouble because of display quality}''. Specifically, the employed HMD obscured parts of the users' field of view ``\textit{preventing the ability of glancing down (on the SW) without moving your head}''. Another issue highlighted by 6 participants was the \textit{cost of focus switching} which refers to the accommodation to different focus depths of the touch screen and the virtual HMD screen with a typical comment being: ``\textit{I have to focus on three layers, which is overwhelming: SW, HMD and real world}''. This also led to \textit{coordination problems across devices} as mentioned by 9 participants. Hence, some participants suggested not to concurrently use HMD and SW as output: ``\textit{Pairing the two devices is good, but use one as input, the other as output, not both as output, it's confusing}''. Also, social concerns of spatial pointing were raised, ``\textit{I could not imagine this in a packed bus}''. 






%\todo[inline]{improve the readability:  more clearly present our finding that combined HMD-mobile interaction can outperform interaction with individual devices}